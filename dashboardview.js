var subscribedTopics = {};
var receivedMessages = 0;


// init knowledge of default topics // NOTE not sure if necessary
subscribedTopics[LWT_TOPIC] = {received: 0};
// subscribedTopics[SYSCLIENTSTOTALTOPIC] = {received: 0};
// subscribedTopics[SYSMESSAGESSENTTOPIC] = {received: 0};
// subscribedTopics[SYSUPTIMETOPIC] = {received: 0};
// subscribedTopics[BRIGHTNESSTOPIC] = {received: 0};
// subscribedTopics[TEMPERATURETOPIC] = {received: 0};

// helper function to check if the dashboard has knowledge of this topic
var topicIsKnown = (topicName) => {
	if(typeof subscribedTopics[topicName] === 'undefined')
		return false;
	else
		return true;
}

// show if client is connected or not
var showConnectionStatus = (primaryText, secondaryText) => {
	$('#connection-status-icon-disconnected').toggle();
	$('#connection-status-icon-connected').toggle();
	$('#connection-status-primary').text(primaryText);
	$('#connection-status-secondary').text(secondaryText);
}

// enable return key to subscribe a topic
$("#add-topic-input").keyup(function(event) {
    if (event.keyCode === 13) {
			addInputTopic();
    }
});

// enable button to subscribe a topic
$('#add-topic-btn').click(() => {
	addInputTopic();
});

// updates the UI when a LWT message was received
var updateLWT = (msg) => {
	var date = new Date();

	var formattedDate = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

	$('#lwt-time').text(formattedDate);
	$('#lwt-message').text(msg.payloadString);
}

// updates GUI for topics other than LWT
var updateUnknownTopics = (msg) => {
	receivedMessages++;

	// TODO WHY? Fehler sonst wenn man topic wieder deabonniert
	if(typeof subscribedTopics[msg.topic] === 'undefined') return;
	var cNum = subscribedTopics[msg.topic].received++;

	// update UI, but not for systopics
	if(sysTopics.indexOf(msg.topic) === -1) {
		var counterElem = document.getElementById('msgs-received-' + msg.topic);
		$(counterElem).text(cNum);
	}

	// find index in piechart
	var index = -1;
	for(var i = 0; i < myDoughnutChart.data.labels.length; i++) {
		if(myDoughnutChart.data.labels[i] === msg.topic) {
			index = i;
			break;
		}
	}

	myDoughnutChart.data.datasets[0].data[index] = cNum
	myDoughnutChart.update();
	$('#msgs-received-num').text(receivedMessages);

	if(predefinedTopics.indexOf(msg.topic) === -1 &&
			sysTopics.indexOf(msg.topic) === -1) {
		var heading = document.getElementById(msg.topic + '-msg-val');
		heading.innerHTML = msg.payloadString;

	}
}
//
// var defaultTopics = {																												// NOTE CHANGED!
// 	'home/brightness': {
// 		color: {
// 			r: 36,
// 			g: 137,
// 			b: 201
// 		}
// 	},
// 	'home/temperature': {
// 		color: {
// 			r: 248,
// 			g: 172,
// 			b: 89
// 		}
// 	}
// }

// color information for default topics
var defaultTopics = {};
defaultTopics[BRIGHTNESSTOPIC] = {
		color: {
			r: 36,
			g: 137,
			b: 201
		}
	};
defaultTopics[TEMPERATURETOPIC] = {
		color: {
			r: 248,
			g: 172,
			b: 89
		}
	};

// subscribe topic and update UI when user added a new topic
var addInputTopic = (topic = null, fromPersistentSession = false) => {

	var input;
	if(topic === null) {
		// get inputs
		input = $("#add-topic-input").val();
		$("#add-topic-input").val('');
	}
	else {
		input = topic;
	}

	if(!isConnected) {
		console.error("Not subscribing, is not connected");
		return;
	}

	var isSysTopic = (sysTopics.indexOf(input) !== -1);

	if(typeof subscribedTopics[input] === 'undefined') {

		var color = 'rgba(218, 218, 218, 1)';
		var transparentColor = 'rgba(218, 218, 218, 0.5)';

		if(defaultTopics[input] !== undefined) {
			var r = defaultTopics[input].color.r;
			var g = defaultTopics[input].color.g;
			var b = defaultTopics[input].color.b;
			color = `rgba(${r},${g},${b},1)`;
			transparentColor = `rgba(${r},${g},${b},0.5)`;
		}

		if(!isSysTopic) {
			myDoughnutChart.data.datasets[0].data.push(0);
			myDoughnutChart.data.datasets[0].backgroundColor.push(transparentColor);
			myDoughnutChart.data.labels.push(input);
			myDoughnutChart.update();
		}

		// create new list entry
		var entry = document.createElement('li');
		entry.setAttribute('class', 'topic-entry')
		var text = $(`<span class="left"><span class="color-identifier glyphicon glyphicon-stop" style="color: ${color}"> </span>${input}</span>`);
		var removeIcon = $(`<div class="icon-right" style="cursor:pointer">
													<span class="glyphicon glyphicon-remove"></span>
												</div>`);

		var newWidget = null;
		var newCounter = null;
		if(!isSysTopic) {

			// update widget status
			if(input === BRIGHTNESSTOPIC) {
				// $('#brightness-sub-status').toggleClass('greyText');
				// $('#brightness-sub-status').toggleClass('primaryText');
				// $('#brightness-sub-status').text('Subscribed');
			}
			else if(input === TEMPERATURETOPIC) {
				// STUB
			}
			else {
				// if unknown topic, create default widget
				// <h6 class="inline alignright flex-center-vertically primaryText" id="brightness-sub-status">Subscribed</h6>
				newWidget = $(`<div class="box default-widget">
					<div class="box-head">
						<div>
							<h5 class="inline alignleft">${input}</h5>
							<div style="clear: both;"></div>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<h3 id="${input}-msg-val">-</h3>
							</div>
						</div>
					</div>
				</div>`);
				$('#grid-wrapper').append(newWidget);
			}

			// also create number of received messages counter next to topic
			// distribution
			newCounter = $(
			`<h1 class="headline-break" style="padding-top: 20px; color: ${transparentColor}" id="msgs-received-${input}">0</h1>
			<small>${input}</small>`
			);
			$('#topic-msg-counter').append(newCounter);

		}

		// register remove event handler
		$(removeIcon).click(() => {
			// unsubscribe topic
			unsubscribeTopic(input);
			// remove parent from ui
			$(removeIcon).parent().remove();
			// drop from subscribed topics array
			delete subscribedTopics[input];

			// update widget status
			if(input === BRIGHTNESSTOPIC) {
				$('#brightness-sub-status').toggleClass('greyText');
				$('#brightness-sub-status').toggleClass('primaryText');
				$('#brightness-sub-status').text('Not Subscribed');
			}

			if(newWidget !== null) {
				newWidget.remove();
			}
			if(newCounter !== null) {
				newCounter.remove();
			}

			if(!isSysTopic) {

				// find index in piechart
				var index = -1;
				for(var i = 0; i < myDoughnutChart.data.labels.length; i++) {
					if(myDoughnutChart.data.labels[i] === input) {
						index = i;
						break;
					}
				}

				myDoughnutChart.data.datasets[0].data.splice(index, 1);
				myDoughnutChart.data.datasets[0].backgroundColor.splice(index, 1);
				myDoughnutChart.data.labels.splice(index, 1);
				myDoughnutChart.update();
			}

		});
		$(entry).append(text);
		$(entry).append(removeIcon);
		$("#active-topics").append(entry);


		if(!fromPersistentSession) {
			// subscribe topic
			subscribeTopic(input);
		}
			// subscribedTopics.push(input);
			subscribedTopics[input] = {received: 0};

	}
	else {
		console.log(`Topic ${input} already subscribed`);
	}
}


// initialize live linechart for brightness
moment.locale('de');
var data = [];
var lastReadIndex = -1;
var ctx = document.getElementById('brightness-chart').getContext('2d');
var myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Brightness',
            data: [],
						fill: false,
            backgroundColor: [
                'rgba(36, 137, 201, 1)'
            ],
            borderColor: [
                'rgba(36, 137,201,1)'
            ],
            borderWidth: 1
        }]
    },
		options: {
	    elements: {
	        line: {
	            tension: 0
	        }
	    },
			plugins: {
      	streaming: {
        	delay: 2000,
        	onRefresh: function(chart) {
	          chart.data.datasets.forEach(function(dataset) {
								var d = Date.now();

								var cIndex = data.length -1;
								var display = null;
								if(typeof data[lastReadIndex] !== 'undefined') {
									display = data[lastReadIndex].brightness;

									// if not subscribed, dont display
									if(typeof subscribedTopics[BRIGHTNESSTOPIC] === 'undefined')
										display = null;

									// if no connection, dont display
									if(!isConnected)
										display = null;

								}
								lastReadIndex = cIndex;
	            	dataset.data.push({
	              	x: d,
	              	y: display
	            });
	          });
        }
      }},
     	legend: {
        display: false
     	},
			scales: {
				yAxes: [{
						stacked: true,
        		ticks: {beginAtZero:true,max:3000}
				}],

      xAxes: [{

        type: 'realtime'

      }]
		},
	}
});

// update the "last received message" widget
updateLastMessage = (msg) => {
	$('#last-topic').text(msg.topic);
	$('#last-message').text(msg.payloadString);
}

// information & functions for the temperature widget
var maxTemperature = -Infinity;
var minTemperature = Infinity;
var lastTemperatures = [];
updateTemperature = (temperature) => {

		lastTemperatures.push(temperature);

		if(lastTemperatures.length > 4)
			lastTemperatures.shift();

		var list = document.getElementById('last-temperatures');
		list.innerHTML = '';

		for(var i = lastTemperatures.length - 1; i >= 0; i--) {
			var li = document.createElement('li');
			var icon = document.createElement('i');
			icon.setAttribute('class', 'fa fa-circle warningText temperature-entry-marking');
			var span = document.createElement('span');
			span.innerHTML = lastTemperatures[i];

			li.appendChild(icon);
			li.appendChild(span);
			list.appendChild(li);
		}


		$('#last-temperature').text(temperature)

		if(temperature > maxTemperature) {
			$('#max-temperature').text(temperature);
			maxTemperature = temperature;
		}

		if(temperature < minTemperature) {
			$('#min-temperature').text(temperature);
			minTemperature = temperature;
		}

}

// additional functions and information for the brightness widget
var maxBrightness = -Infinity;
var minBrightness = Infinity;
updateBrightness = (brightness) => {

	$('#last-brightness').text(brightness)

	if(brightness > maxBrightness) {
		$('#max-brightness').text(brightness);
		maxBrightness = brightness;
	}

	if(brightness < minBrightness) {
		$('#min-brightness').text(brightness);
		minBrightness = brightness;
	}

	data.push({
		brightness: brightness,
		displayed: false
	});
}

// initialize donut charge to show distribution of subscribed topics
var doughnutChart = document.getElementById('topic-distribution-pie-chart');
doughnutChart.height = 100;
var myDoughnutChart = new Chart(doughnutChart, {
    type: 'doughnut',
    data: {
		    datasets: [
					{
		        data: [],
						backgroundColor: [],
		    }
			],
			labels: []
		},
		options: {
			scales:{
			  yAxes: [],
			  xAxes: []
			},
      responsive: true,
      legend: {
          display: false,
          position: "bottom",
          labels: {
              fontColor: "#333",
              fontSize: 16
          }
      },
	    tooltips: {
	         enabled: false
	    }
    }
});
