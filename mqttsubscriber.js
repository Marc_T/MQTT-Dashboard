////////////////////////////////////////////////////////////////////////////////
// MQTT-Information
const BROKER = '10.50.14.203';
// const BROKER = 'broker.hivemq.com';
const LWT_TOPIC = "home/lwt";

// default $SYS topics
const SYSCLIENTSTOTALTOPIC = '$SYS/broker/clients/total';
const SYSMESSAGESSENTTOPIC = '$SYS/broker/messages/sent';
const SYSUPTIMETOPIC = '$SYS/broker/uptime';
// const SYSCLIENTSTOTALTOPIC = 'fs/total';
// const SYSMESSAGESSENTTOPIC = 'fs/sent';
// const SYSUPTIMETOPIC = 'fs/upt';

// topics specifically supported by the dashboard:
const BRIGHTNESSTOPIC = 'home/brightness';
const TEMPERATURETOPIC = 'home/temperature';

// create client with callback handlers & connect to broker
// using a persistent session (i.e. to regain previously
// subscribed topics on reconnect)
// var client = new Paho.MQTT.Client(BROKER, Number(8000), "97e1e59c");
var client = new Paho.MQTT.Client(BROKER, Number(9001), "97e1e59c");
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;
client.connect({onSuccess:onConnect, cleanSession: false});

// -----------------------------------------------------------------------------
// MQTT-Callback functions

function onConnect() {
  // set internal connection status
  console.log("onConnect");
  isConnected = true;

  // update UI
  showConnectionStatus("Connected", `to ${BROKER}`);
  $('#reconnect-btn').hide();

  // subscribe LWT topic and some default $SYS topics
	client.subscribe(LWT_TOPIC);
  addInputTopic(SYSCLIENTSTOTALTOPIC);
  addInputTopic(SYSMESSAGESSENTTOPIC);
  addInputTopic(SYSUPTIMETOPIC);
}

function onConnectionLost(responseObject) {
  // connect return code 0 = connection accepted
  // => if connection was not accepted show option to reconnect manually
  if (responseObject.errorCode !== 0) {
    // set internal connection status
    isConnected = false;
    console.log("onConnectionLost:" + responseObject.errorMessage);

    // Update UI
    showConnectionStatus("Disconnected", "Subscriber is not connected");
    $('#reconnect-btn').show();
  }
}

function onMessageArrived(message) {
  // when a message arrives, process it based on its topic
  // console.log(message.topic);

  // if the dasboard does not have knowledge of the received topic,
  // then it's reception is the result of a persistent session
  // => inform the dashboard
  if(!topicIsKnown(message.topic))
    addInputTopic(message.topic, true);


  // show last received message
  updateLastMessage(message);

  if(message.topic !== LWT_TOPIC)
    updateUnknownTopics(message);
  else
    updateLWT(message);

  // Update the GUI for the default $SYS-topics
  if(message.topic === SYSCLIENTSTOTALTOPIC)
    $('#sys-clients').text(message.payloadString);
  else if(message.topic === SYSMESSAGESSENTTOPIC)
    $('#sys-msgs-sent').text(message.payloadString);
  else if(message.topic === SYSUPTIMETOPIC)
    $('#sys-uptime').text(message.payloadString);

  // Update GUI for
	if(message.topic === BRIGHTNESSTOPIC)
		updateBrightness(parseInt(message.payloadString));
  else if(message.topic === TEMPERATURETOPIC)
		updateTemperature(parseInt(message.payloadString));
}
////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////
// Dashboard-Information
var isConnected = false;
var predefinedTopics = [BRIGHTNESSTOPIC, LWT_TOPIC, TEMPERATURETOPIC];
var sysTopics = [SYSCLIENTSTOTALTOPIC, SYSMESSAGESSENTTOPIC, SYSUPTIMETOPIC];

// -----------------------------------------------------------------------------
// Reconnect button

$('#reconnect-btn').click(() => {
  // connect the client
  //NOTE cleansession to continue using previously subscribed topics
  client.connect({onSuccess:onConnect, cleanSession: false});
});
////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////
// Wrapper-functions
function subscribeTopic(topic) {
	client.subscribe(topic);
}

function unsubscribeTopic(topic) {
	client.unsubscribe(topic);
}
////////////////////////////////////////////////////////////////////////////////
